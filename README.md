#Crypto Speed Tests (for Python)

This package has one very simple goal:

**Compare the speed of PyCrypto and cryptography cryptographic libraries for Python**

In particular, the only currently implemented test is a comparison of RSA PKCS1_v1.5 signing and verification which can be ran by running `test_runner.py`

## Installing Requirements

Easy as pie with pip: `pip install -r requirements.txt`

## Running the Tests

Also, easy as pie... `python test_runner.py`

## Results
Library        |Version | Exec n=20000
---------------|--------|--------------
PyCrypto       |2.6.1   |      68.3203s
cryptography   |0.6.1   |      46.6717s

By the above numbers, cryptography is faster than PyCrypto by 31.6869%.

The above excecution was on a Intel(R) Core(TM) i7-2675QM CPU @ 2.20GHz running Ubuntu 14.10 (Utopic)
