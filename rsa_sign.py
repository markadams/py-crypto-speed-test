from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import interfaces, hashes
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.backends import default_backend
from cryptography.exceptions import InvalidSignature

import string
import random
import base64
import timeit


def read_keys(filename='keys/rsa_priv.key'):
    key_data = open(filename, 'r').read().encode('ascii')

    pycrypto_key = RSA.importKey(key_data)
    crypto_key = load_pem_private_key(key_data, password=None, backend=default_backend())

    return pycrypto_key, crypto_key


def sign_pycrypto(msg, key):
    return PKCS1_v1_5.new(key).sign(SHA256.new(msg))


def verify_pycrypto(msg, key, sig):
    return PKCS1_v1_5.new(key).verify(SHA256.new(msg), sig)


def sign_crypto(msg, priv_key):
    signer = priv_key.signer(
        padding.PKCS1v15(),
        hashes.SHA256()
    )

    signer.update(msg)
    return signer.finalize()


def verify_crypto(msg, pub_key, sig):
    verifier = pub_key.verifier(
        sig,
        padding.PKCS1v15(),
        hashes.SHA256()
    )

    verifier.update(msg)

    try:
        verifier.verify()
        return True
    except InvalidSignature:
        return False


def get_random_msg(n=100):
    return str([ random.choice(string.printable)
        for x in range(n) ]).encode('ascii')


def pycrypto_sign_and_verify(key, msg=None):
    msg = msg or get_random_msg()
    sig = sign_pycrypto(msg, key)
    verified = verify_pycrypto(msg, key, sig)

    return verified, sig


def crypto_sign_and_verify(key, msg=None):
    msg = msg or get_random_msg()
    sig = sign_crypto(msg, key)
    verified = verify_crypto(msg, key.public_key(), sig)

    return verified, sig


def sanity_check():
    pyc_key, crypto_key = read_keys()
    msg = get_random_msg()

    verified_pyc, sig_pyc = pycrypto_sign_and_verify(pyc_key, msg)
    verified_crypto, sig_crypto = crypto_sign_and_verify(crypto_key, msg)

    assert verified_pyc
    assert verified_crypto

    assert sig_pyc == sig_crypto
