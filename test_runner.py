from rsa_sign import sanity_check
import timeit
import Crypto
import cryptography

setup_func = """\
import rsa_sign
pyc_key, crypto_key = rsa_sign.read_keys()
"""

if __name__ == '__main__':
    sanity_check()

    n = 20000
    
    pyc_result = timeit.timeit('rsa_sign.pycrypto_sign_and_verify(pyc_key)',
        setup=setup_func, number=n)

    crypto_result = timeit.timeit('rsa_sign.crypto_sign_and_verify(crypto_key)',
        setup=setup_func, number=n)

    faster_time = min(pyc_result, crypto_result)
    slower_time = max(pyc_result, crypto_result)

    faster = 'PyCrypto' if pyc_result == faster_time else 'cryptography'
    slower = 'PyCrypto' if pyc_result == slower_time else 'cryptography'

    perc = 1 - (faster_time / slower_time)
    
    print('{0:<14} {1:<10} {2:.4f}'.format('PyCrypto', Crypto.__version__, pyc_result))
    print('{0:<14} {1:<10} {2:.4f}'.format('cryptography', cryptography.__version__, crypto_result))
    
    print('{0} is faster than {1} by {2:.4%}'.format(faster, slower, perc))

